# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  (arr.min...arr.max).to_a.count
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  arr.sort == arr
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  vowels = %w[a e i o u]
  str.chars.count {|let| vowels.include?(let.downcase)}
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  str.delete('aeiouAEIOU')
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  int.to_s.chars.sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  count = 0
  str.chars.each do |let|
    str.chars.each do |let2|
      count += 1 if let.downcase == let2.downcase
    end

    return true if count > 1
    count = 0
  end
  false
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  answer = []
  one = []
  two = []
  three = []

  (arr.length).times do |idx|
    if idx < 3
      one << arr[idx]
    elsif idx < 6
      two << arr[idx]
    else
      three << arr[idx]
    end
  end

  answer << ("(#{one[0]}#{one[1]}#{one[2]})")
  answer << ("#{two[0]}#{two[1]}#{two[2]}")
  answer = answer.join(" ")
  answer << ("-#{three[0]}#{three[1]}#{three[2]}#{three[3]}")
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  str = str.delete(",")
  str = str.chars.map {|num| num.to_i}
  str.max - str.min
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  if offset >= 1
    offset.times do
      holder = arr.shift
      arr << holder
    end
  elsif offset < 1
    offset.abs.times do
      holder = arr.pop
      arr.unshift(holder)
    end
  end
  arr
end
